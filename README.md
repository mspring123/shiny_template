# generating template dashboard
The following code snippets & coding is just some plain r code. It's only purpose is to contain the code as reminder. 

> author: michael spring
> > creating date: 18.09.2020
> > > some basic & generic shiny dashboard


```plantuml
!define ICONURL https://raw.githubusercontent.com/tupadr3/plantuml-icon-font-sprites/v2.1.0
skinparam defaultTextAlignment center
!include ICONURL/common.puml
!include ICONURL/font-awesome-5/gitlab.puml
!include ICONURL/font-awesome-5/java.puml
!include ICONURL/font-awesome-5/rocket.puml
!include ICONURL/font-awesome/newspaper_o.puml
FA_NEWSPAPER_O(news,good news!,node) #White {
FA5_GITLAB(gitlab,GitLab.com,node) #White
FA5_JAVA(java,PlantUML,node) #White
FA5_ROCKET(rocket,Integrated,node) #White
}
gitlab ..> java
java ..> rocket
```



```
library(httr)
library(jsonlite)
#'@description Class for api interaction
OapiClass <- setRefClass(
  "OapiClass",
  fields = c(api_table = "character", cfield = "numeric"),
  methods = c(
    genus = function(level, ...) {
      strsplit(apitable, " ")[[1]][1]
    },
    multiple = function() {
      1 < count
    },
    request_to_response = function(api_table) {
      path <- "https://ssd-api.jpl.nasa.gov/"
      constring <-
        paste(... = path,
              api_table,
              collapse = NULL,
              sep = "/")
      return(constring)
    }
  )
)
#'@OapiClass generate new object
conobj <- OapiClass$new()
apiviewstr <<-
  conobj$request_to_response("sentry.api") # interact with object
#conobj2 <- OapiClass$new()


#'@method getdata, performs full rest api request
library(jsonlite)
getdata <- function(o_path) {
  r <- GET(url = o_path)
  # status_code(r)
  
  if (status_code(r) == 200) {
    print("request was successful")
  } else {
    stopifnot(status_code(r) != 200)
    print(str(404))
  }
  #str(content(r))
  #'@field r, coverrides the variable r
  r <- content(r, as = "text", encoding = "UTF-8")
  return(df <- fromJSON(r, flatten = TRUE))
  print(head(df, 12))
}

#'@method get api data & return as datatable
apiview <- getdata(apiviewstr)
head(apiview$data, 12) # obtain 12 first records
```

asdf
